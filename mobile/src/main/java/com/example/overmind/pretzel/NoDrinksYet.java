package com.example.overmind.pretzel;

import android.app.Activity;
import android.os.Bundle;

public class NoDrinksYet extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_drinks_yet);
    }
}
