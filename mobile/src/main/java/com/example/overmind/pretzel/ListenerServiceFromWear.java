package com.example.overmind.pretzel;

/**
 * Created by Hoi on 12/1/2015.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.charset.StandardCharsets;

public class ListenerServiceFromWear extends WearableListenerService {

    private static final String START = "/startsession";
    private static final String COUNT = "/addDrink";
    private static final String TAG = "WearListener";
    private static final String SHAKE = "/shake";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        /*
         * Receive the message from wear
         */
        if (messageEvent.getPath().equals(START)) {
            Intent i = new Intent(this, AddDrinks.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        } else if(messageEvent.getPath().equals(COUNT)) {
            String value = new String(messageEvent.getData(), StandardCharsets.UTF_8);
            SharedPreferences prefs = this.getSharedPreferences(
                    "com.example.app", Context.MODE_PRIVATE);
            String[] list = value.split(" ");
            prefs.edit().putInt("drinkcount", Integer.valueOf(list[0])).apply();
            prefs.edit().putString("type", list[1]).apply();
        } else if(messageEvent.getPath().equals(SHAKE)) {
            Toast.makeText(getApplicationContext(),
                    "User is shaking the watch!!",
                    Toast.LENGTH_LONG).show();
        } else if(messageEvent.getPath().equals("/uber")) {
            Intent i = new Intent(this, UberCalling.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
    }
}