package com.example.overmind.pretzel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;

public class MainActivity extends Activity {


    private GoogleApiClient mGoogleApiClient;

    //======================================= Mer's code  =======================================
    private String userName;
    private ArrayList<String> friends;
    private Integer profileId;
    ListView listView ;
    //======================================= Mer's code  =======================================




    // Mere's code
    //LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
    ArrayList<String> listItems=new ArrayList<String>();
    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
    ArrayAdapter<String> adapter;
    //RECORDING HOW MANY TIMES THE BUTTON HAS BEEN CLICKED
    int clickCounter=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //======================================= Mer's code  =======================================
        generateView();

        listView = (ListView) findViewById(R.id.listView);
        String[] values = new String[] { "Start Session", "Friends",  "Uber"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);
                if (itemValue == "Start Session") {

                    AddDrinks();

                }
                else if (itemValue == "Friends"){
                    Friends();
                }else if (itemValue == "Uber" ){
                    Uber();
                }
            }

        });
        //======================================= Mer's code  =======================================




        //======================================= HOI's code  =======================================
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {

                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                    }
                })
                .addApi(Wearable.API)
                .addApi(LocationServices.API)
                .build();
        //================================   HOI's code ===================================================
    }


    //======================================= Mer's code  =======================================
    private void generateView(){
        userName = Global.userName;
        friends = Global.friends;
        profileId = Global.nameToPhotos.get(userName);
    }

    private void Uber(){
        Intent i = new Intent(this,UberCalling.class);
        startActivity(i);
    }


    private void AddDrinks() {
        sendNotification("", "", "/startsession");
        SharedPreferences prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        prefs.edit().putInt("drinkcount", 0).apply();
        Intent i = new Intent(this, AddDrinks.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    private void Friends(){
        Intent i = new Intent(this,Friends.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }
    //======================================= Mer's code  =======================================


    //======================================= HOI's code  =======================================

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendNotification(String title, String content, String tag) {
        if (mGoogleApiClient.isConnected()) {
            if (tag.equals( "/notification")) {
                PutDataMapRequest dataMapRequest = PutDataMapRequest.create("/notification");
                dataMapRequest.getDataMap().putDouble("timestamp", System.currentTimeMillis());
                dataMapRequest.getDataMap().putString("title", title);
                dataMapRequest.getDataMap().putString("content", content);
                PutDataRequest putDataRequest = dataMapRequest.asPutDataRequest();
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataRequest);
            } else if (tag.equals("/startsession")) {
                PutDataMapRequest dataMapRequest = PutDataMapRequest.create("/startsession");
                dataMapRequest.getDataMap().putDouble("timestamp", System.currentTimeMillis());
                PutDataRequest putDataRequest = dataMapRequest.asPutDataRequest();
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataRequest);
            }

        }
        else {
        }
    }

// ======================================= HOI's code END =======================================









}
