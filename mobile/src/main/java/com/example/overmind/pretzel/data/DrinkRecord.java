package com.example.overmind.pretzel.data;

import java.util.Calendar;

/**
 * Created by danielpok on 12/9/15.
 */
public class DrinkRecord {
    public long timestamp;
    public String drinkType;
    public int drinkDrawable;

    public DrinkRecord(long timestamp, String drinkType, int drinkDrawable){
        this.timestamp = timestamp;
        this.drinkType = drinkType;
        this.drinkDrawable = drinkDrawable;
    }

    public DrinkRecord(String drinkType, int drinkDrawable){
        this(System.currentTimeMillis(), drinkType, drinkDrawable);
    }
}
