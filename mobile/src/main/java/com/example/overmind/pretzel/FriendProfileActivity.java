package com.example.overmind.pretzel;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.overmind.pretzel.data.FriendProfile;

public class FriendProfileActivity extends Activity {

    public final static String TAG = "FriendProfileActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friend_profile);

        ImageView img= (ImageView) findViewById(R.id.friend_profile_pic);
        TextView name = (TextView) findViewById(R.id.friend_name_txt);
        TextView sub = (TextView) findViewById(R.id.friend_sub_txt);
        img.setImageResource(R.drawable.candice);

        int friendNumber = getIntent().getIntExtra(Global.INTENT_FRIEND_ID, -1);

        if(friendNumber == -1){
            img.setImageResource(R.drawable.meicon);
            name.setText("Friend Name");
            sub.setText("Drinking Status");
        } else {
            FriendProfile friend = Global.friendList.get(friendNumber);
            img.setImageResource(friend.profileDrawable);
            name.setText(friend.name);
            sub.setText(String.format("BAC: %.3f", friend.bac));
        }

    }
}
