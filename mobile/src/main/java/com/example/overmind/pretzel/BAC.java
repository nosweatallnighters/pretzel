package com.example.overmind.pretzel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

public class BAC extends Activity {
    private EditText text, text2;
    double rFemales = 0.66;
    double rMales = 0.73;
    double weight = 120;
    double unit_alcohol = 5 ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bac);
        // Get the message from the intent
        Intent intent = getIntent();
        int drinks = intent.getIntExtra("drinks", 0);

        Log.d("DEBUG","drink number got");
        text2 = (EditText) findViewById(R.id.editText2);
        text = (EditText) findViewById(R.id.editText);
        double bac = calBAC(drinks);
        String output = "" +bac ;
        String output2 = "" + safeCheck(bac);
        text.setText(output);
        text2.setText(output2);


    }

    private  double  calBAC(int drinks){
        /***
         * BAC calculator Formula:
         BAC% = (A × 5.14/W × r) - .015 × H
         */
        Log.d("DEBUG","" + drinks);
        int H = 0;
        return (unit_alcohol*(drinks-1)/weight*rFemales) - 0.015* H;
    }

    private  String safeCheck(double bac){
        if (bac <= 0.08 ){
            return "It's legal to drive";
        }else{
            return "Do not drive";
        }

    }


}
