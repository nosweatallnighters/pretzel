package com.example.overmind.pretzel;

import com.example.overmind.pretzel.data.FriendProfile;
import com.example.overmind.pretzel.data.LocalUserProfile;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by junhuizhang on 12/6/15.
 */
public class Global {
    public static String userName;
    public static ArrayList<String> friends;
    public static HashMap<String,Integer> nameToPhotos;
    public static LocalUserProfile user;
    public static ArrayList<FriendProfile> friendList;

    public final static String INTENT_FRIEND_ID = "INTENT_FRIEND_ID";

    private static ArrayList<String> names;
    private static Random ranName;


    static{
        friendList = new ArrayList<>();
    }

    static{
        friends = new ArrayList<>();
        friends.add("Justin Bieber");
        friends.add("Candice Swanepoel");
        friends.add("Harry Styles");
        friends.add("Alessandra Ambrosio");
        friends.add("Adam Levine");
        friends.add("Adriana Lima");
    }

    static {
        nameToPhotos = new HashMap<>();
        nameToPhotos.put("Justin Bieber", R.drawable.justin);
        nameToPhotos.put("Candice Swanepoel", R.drawable.candice);
        nameToPhotos.put("Harry Styles", R.drawable.harry);
        nameToPhotos.put("Alessandra Ambrosio", R.drawable.alessandra);
        nameToPhotos.put("Adam Levine", R.drawable.adam);
        nameToPhotos.put("Adriana Lima", R.drawable.adriana);
    }

    static {
        ranName = new Random();
    }





    public static LocalUserProfile generate(){
        if(user != null){
            return user;
        }

        userName = friends.get(ranName.nextInt(6));
        friends.remove(userName);
        user = new LocalUserProfile();
        user.name = userName;
        user.profileDrawable = nameToPhotos.get(userName);
        user.bac = 0.0f;
        user.age = 22;
        user.weightKG = 70;
        user.sex = LocalUserProfile.SEX_FEMALE;
        return user;
    }

    public static FriendProfile makeFriend(){
        if(friends.size() == 0){
            return null;
        }
        String name = friends.get(ranName.nextInt(friends.size()));
        friends.remove(name);
        FriendProfile profile = new FriendProfile();
        profile.name = name;
        profile.profileDrawable = nameToPhotos.get(name);
        profile.bac = ranName.nextFloat() * 0.10f;
        return profile;
    }

}
