package com.example.overmind.pretzel;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.overmind.pretzel.data.DrinkRecord;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by danielpok on 12/9/15.
 */
public class DrinkListAdapter extends BaseAdapter implements ListAdapter {
    ArrayList<DrinkRecord> drinksList;
    Context mContext;
    LayoutInflater mLayoutInflater;

    public DrinkListAdapter(Context context, ArrayList<DrinkRecord> drinks){
        mContext = context;
        drinksList = drinks;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return drinksList.size();
    }

    @Override
    public DrinkRecord getItem(int position) {
        return drinksList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return drinksList.get(position).hashCode();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if(convertView != null){
            rowView = convertView;
        } else {
            rowView = mLayoutInflater.inflate(R.layout.drink_list_row, parent, false);
        }
        final DrinkRecord target = getItem(position);
        if(rowView.getTag() == target){
            return rowView;
        } else {
            ImageView profilePic = (ImageView) rowView.findViewById(R.id.friend_profile_pic);
            final TextView friendName = (TextView) rowView.findViewById(R.id.friend_name_txt);
            TextView friendSub = (TextView) rowView.findViewById(R.id.friend_sub_txt);
            profilePic.setImageResource(target.drinkDrawable);
            friendName.setText(new SimpleDateFormat("HH:mm").format(new Date(target.timestamp)));
            friendSub.setText(target.drinkType);
            rowView.setTag(target);
            return rowView;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return drinksList.isEmpty();
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }


}
