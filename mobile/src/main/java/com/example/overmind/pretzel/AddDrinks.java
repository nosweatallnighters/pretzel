package com.example.overmind.pretzel;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.overmind.pretzel.data.DrinkRecord;
import com.example.overmind.pretzel.data.LocalUserProfile;

import java.util.ArrayList;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.*;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

public class AddDrinks extends Activity implements OnSharedPreferenceChangeListener  {
    ArrayList<DrinkRecord> mDrinkList;
    ListView mDrinkListView;
    DrinkListAdapter mDrinkListAdapter;
    ImageButton mAddBeerButton;
    ImageButton mAddWineButton;
    ImageButton mAddLiqueurButton;
    TextView mDrinkCountLabel;
    TextView mMessageOne;
    TextView mMessageTwo;
    LocalUserProfile mUser;
    private GoogleApiClient mGoogleApiClient;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_drinks);
        mDrinkListView = (ListView) findViewById(R.id.drink_list);
        mDrinkListView.setEmptyView(findViewById(R.id.drinks_empty));
        mAddBeerButton = (ImageButton) findViewById(R.id.add_beer_button);
        mAddWineButton = (ImageButton) findViewById(R.id.add_wine_button);
        mAddLiqueurButton = (ImageButton) findViewById(R.id.add_liqueur_button);
        mDrinkCountLabel = (TextView) findViewById(R.id.drink_count_text);
        mMessageOne = (TextView) findViewById(R.id.drink_message_one);
        mMessageTwo = (TextView) findViewById(R.id.drink_message_two);
        prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        prefs.registerOnSharedPreferenceChangeListener(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {

                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                    }
                })
                .addApi(Wearable.API)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart(){
        super.onStart();
        mGoogleApiClient.connect();
        prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        prefs.registerOnSharedPreferenceChangeListener(this);
        mUser = Global.generate();
        mDrinkList = mUser.drinks;
        mDrinkCountLabel.setText(String.format("%d", mUser.getDrinkCount()));
        mMessageOne.setText(mUser.getRecommendation().text);
        mMessageTwo.setText(String.format("Estimated BAC: %.3f", mUser.getBAC()));
        mDrinkListAdapter = new DrinkListAdapter(getApplicationContext(), mDrinkList);
        mDrinkListView.setAdapter(mDrinkListAdapter);
        mAddBeerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDrink(new DrinkRecord("Beer", R.drawable.beer));
            }
        });
        mAddWineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addDrink(new DrinkRecord("Wine", R.drawable.wine));
            }
        });
        mAddLiqueurButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDrink(new DrinkRecord("Liqueur", R.drawable.liquor));
            }
        });
        sendDrinks();
    }

    public void addDrink(DrinkRecord drink){
        addDrink(drink, true);
    }

    public void addDrink(DrinkRecord drink, boolean shouldSend){
        this.mDrinkList.add(0, drink);
        this.mDrinkListAdapter.notifyDataSetChanged();
        mDrinkCountLabel.setText(String.format("%d", mUser.getDrinkCount()));
        mMessageOne.setText(mUser.getRecommendation().text);
        mMessageTwo.setText(String.format("Estimated BAC: %.3f", mUser.getBAC()));
        if(shouldSend){
            prefs.edit().putInt("drinkcount", mDrinkList.size()).apply();
            sendDrinks();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void sendDrinks() {
        if (mGoogleApiClient.isConnected()) {
            PutDataMapRequest dataMapRequest = PutDataMapRequest.create("/drinkcount");
            dataMapRequest.getDataMap().putDouble("timestamp", System.currentTimeMillis());
            dataMapRequest.getDataMap().putInt("drinkcount", prefs.getInt("drinkcount", 0));
            PutDataRequest putDataRequest = dataMapRequest.asPutDataRequest();
            Wearable.DataApi.putDataItem(mGoogleApiClient, putDataRequest);
        }
        else {
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1)
    {
        Log.d("DEBUG", "Preference Changed");
        if(arg1.equals("drinkcount")) {
            int numDrinks = arg0.getInt(arg1, 0);
            if (numDrinks == mDrinkList.size()) {
                return;
            }
            String type = arg0.getString("type", "beer");
            DrinkRecord drink;
            if(type.equals("liquor")){
                addDrink(new DrinkRecord("Beer", R.drawable.beer), false);
            } else if (type.equals("wine")){
                addDrink(new DrinkRecord("Wine", R.drawable.wine), false);
            } else if ((type.equals("beer"))){
                addDrink(new DrinkRecord("Liqueur", R.drawable.liquor), false);
            }
        }
    }
}

