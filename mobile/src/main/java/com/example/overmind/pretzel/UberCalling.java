
package com.example.overmind.pretzel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.uber.sdk.android.rides.RideParameters;
import com.uber.sdk.android.rides.RequestButton;


/**
 * Activity that demonstrates how to use a {@link RequestButton}.
 */
public class UberCalling extends AppCompatActivity {

    private static final String DROPOFF_ADDR = "Toby's Apt";
    private static final float DROPOFF_LAT = 37.866712f;
    private static final float DROPOFF_LONG = -122.2674027f;
    private static final String DROPOFF_NICK = "";

    private static final String PICKUP_ADDR = "";
    private static final float PICKUP_LAT = 37.8757151f;
    private static final float PICKUP_LONG = -122.2590485f;
    private static final String PICKUP_NICK = "Soda Hall";
    private static final String UBERX_PRODUCT_ID = "a1111c8c-c720-46c3-8534-2fcdd730040d";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uber_calling);

        RequestButton uberButtonBlack = (RequestButton) findViewById(R.id.uber_button_black);
        TextView usless = (TextView) findViewById(R.id.uber_icon);

        Toast msg = Toast.makeText(getBaseContext(), "Here is your pickup location! Press the button to call uber", Toast.LENGTH_LONG);
        msg.show();

        TextView info =(TextView) findViewById(R.id.confirm_uber);
        String conversation ="Pickup Location:";
        info.setText(conversation);

        TextView location = (TextView) findViewById(R.id.location);
        conversation ="Soda Hall";
        location.setText(conversation);


        RideParameters rideParameters = new RideParameters.Builder()
                .setProductId(UBERX_PRODUCT_ID)
                .setPickupLocation(PICKUP_LAT, PICKUP_LONG, PICKUP_NICK, PICKUP_ADDR)
                .setDropoffLocation(DROPOFF_LAT, DROPOFF_LONG, DROPOFF_NICK, DROPOFF_ADDR)
                .build();
        uberButtonBlack.setRideParameters(rideParameters);
    }
}










