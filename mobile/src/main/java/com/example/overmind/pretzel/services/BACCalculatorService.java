package com.example.overmind.pretzel.services;

import com.example.overmind.pretzel.data.FriendProfile;

/**
 * Created by danielpok on 12/9/15.
 */
public class BACCalculatorService {

    public static final double BW_FEMALE = 0.49;
    public static final double BW_MALE = 0.58;
    public static final double MEAN_MR_FEMALE = 0.017;
    public static final double MEAN_MR_MALE = 0.015;

    public static class Recommendation{
        public String text;
        public int severity;

        public static final int SEVERITY_NONE = 0;
        public static final int SEVERITY_LOW = 1;
        public static final int SEVERITY_MODERATE = 2;
        public static final int SEVERITY_HIGH = 3;
        public static final int SEVERITY_CRITICAL = 4;

        public Recommendation(){
            text = "";
            severity = SEVERITY_NONE;
        }

        public Recommendation(String message, int severity){
            this.text = message;
            this.severity = severity;
        }
    }

    public static double calculateBAC(int standardDrinks, boolean isMale,
                                      double weightKG, double drinkingPeriodHours){
        return Math.max((0.806 * standardDrinks * 1.2) / ((isMale ? BW_MALE : BW_FEMALE) * weightKG)
                - ((isMale ? MEAN_MR_MALE : MEAN_MR_FEMALE) * drinkingPeriodHours), 0);
    }

    public static Recommendation getRecommendation(FriendProfile person){
        return getRecommendation(person.getBAC(), person.isLocalUser, person.age, person.name);
    }

    public static Recommendation getRecommendation(double bac, boolean isLocalUser, int age,
                                                   String name){
        if(age >= 21){
            return getRecommendation21AndOver(bac, isLocalUser, name);
        } else if (age >= 18){
            return getRecommendationUnder21(bac, isLocalUser, name);
        } else {
            return getRecommendationUnder18(bac, isLocalUser, name);
        }
    }

    public static Recommendation getRecommendationUnder18(double bac, boolean isLocalUser,
                                                          String name){
        if(isLocalUser){
            if(bac >= 0.001){
                return new Recommendation(
                        "You are probably okay, but be careful!",
                        Recommendation.SEVERITY_LOW
                );
            } else if(bac >= 0.01){
                return new Recommendation(
                        "You are probably okay, but may get a DUI if you drive.",
                        Recommendation.SEVERITY_MODERATE
                );
            } else if(bac >= 0.04){
                return new Recommendation(
                        "You are in DUI range for your age group. How about getting a ride?",
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.20){
                return new Recommendation(
                        "Now might be a good time to go home. How about an Uber?",
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.40){
                return new Recommendation(
                        "Seek immediate medical attention.",
                        Recommendation.SEVERITY_CRITICAL
                );
            } else {
                return new Recommendation(
                        String.format("You haven't had very much yet, but be careful."),
                        Recommendation.SEVERITY_NONE
                );
            }
        } else {
            // is friend
            if(bac >= 0.01){
                return new Recommendation(
                        String.format("%s is probably okay but may get a DUI if they drive.", name),
                        Recommendation.SEVERITY_LOW
                );
            } else if(bac >= 0.05){
                return new Recommendation(
                        String.format(
                                "Maybe %s should take a break? They are likely to get a DUI if they drive.",
                                name),
                        Recommendation.SEVERITY_MODERATE
                );
            } else if(bac >= 0.08){
                return new Recommendation(
                        String.format("%s definitely shouldn't drive.", name),
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.20){
                return new Recommendation(
                        String.format("%s can't drive and may need extra attention.", name),
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.40){
                return new Recommendation(
                        String.format("%s needs medical attention!", name),
                        Recommendation.SEVERITY_CRITICAL
                );
            } else {
                return new Recommendation(
                        String.format("Hasn't had very much yet", name),
                        Recommendation.SEVERITY_NONE
                );
            }
        }
    }

    public static Recommendation getRecommendationUnder21(double bac, boolean isLocalUser,
                                                          String name){
        if(isLocalUser){
            if(bac >= 0.01){
                return new Recommendation(
                        "You're doing fine, but be aware that you may be in DUI range.",
                        Recommendation.SEVERITY_MODERATE
                );
            } else if(bac >= 0.05){
                return new Recommendation(
                        "Take a break, maybe? You are probably in DUI range.",
                        Recommendation.SEVERITY_MODERATE
                );
            } else if(bac >= 0.08){
                return new Recommendation(
                        "You are likely in DUI range! How about an Uber?",
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.20){
                return new Recommendation(
                        "Now might be a good time to go home. How about an Uber?",
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.40){
                return new Recommendation(
                        "Seek immediate medical attention.",
                        Recommendation.SEVERITY_CRITICAL
                );
            } else {
                return new Recommendation(
                        String.format("You haven't had very much yet"),
                        Recommendation.SEVERITY_NONE
                );
            }
        } else {
            // is friend
            if(bac >= 0.01){
                return new Recommendation(
                        String.format("%s is probably okay but may get a DUI if they drive.", name),
                        Recommendation.SEVERITY_LOW
                );
            } else if(bac >= 0.05){
                return new Recommendation(
                        String.format(
                                "Maybe %s should take a break? They are likely to get a DUI if they drive.",
                                name),
                        Recommendation.SEVERITY_MODERATE
                );
            } else if(bac >= 0.08){
                return new Recommendation(
                        String.format("%s definitely shouldn't drive.", name),
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.20){
                return new Recommendation(
                        String.format("%s can't drive and may need extra attention.", name),
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.40){
                return new Recommendation(
                        String.format("%s probably needs medical attention", name),
                        Recommendation.SEVERITY_CRITICAL
                );
            } else {
                return new Recommendation(
                        String.format("Hasn't had very much yet", name),
                        Recommendation.SEVERITY_NONE
                );
            }
        }
    }

    public static Recommendation getRecommendation21AndOver(double bac, boolean isLocalUser,
                                                          String name){
        if(isLocalUser){
            if(bac < 0.01){
                return new Recommendation(
                        String.format("You haven't had very much yet"),
                        Recommendation.SEVERITY_NONE
                );
            }
            if(bac <= 0.05){
                return new Recommendation(
                        "You're doing fine, but stay safe!",
                        Recommendation.SEVERITY_LOW
                );
            } else if(bac <= 0.08){
                return new Recommendation(
                        "Be extra careful if you drive!",
                        Recommendation.SEVERITY_MODERATE
                );
            } else if(bac <= 0.20){
                return new Recommendation(
                        "You might be in DUI range! How about an Uber?",
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac <= 0.40){
                return new Recommendation(
                        "Now might be a good time to go home. How about an Uber?",
                        Recommendation.SEVERITY_HIGH
                );
            } else {
                return new Recommendation(
                        "Seek immediate medical attention.",
                        Recommendation.SEVERITY_CRITICAL
                );
            }
        } else {
            // is friend
            if(bac >= 0.01){
                return new Recommendation(
                        String.format("%s should be careful driving.", name),
                        Recommendation.SEVERITY_LOW
                );
            } else if(bac >= 0.05){
                return new Recommendation(
                        String.format("%s probably shouldn't drive.", name),
                        Recommendation.SEVERITY_MODERATE
                );
            } else if(bac >= 0.08){
                return new Recommendation(
                        String.format("%s definitely shouldn't drive.", name),
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.20){
                return new Recommendation(
                        String.format("%s can't drive and may need extra attention.", name),
                        Recommendation.SEVERITY_HIGH
                );
            } else if(bac >= 0.40){
                return new Recommendation(
                        String.format("%s probably needs medical attention", name),
                        Recommendation.SEVERITY_CRITICAL
                );
            } else {
                return new Recommendation(
                        String.format("Hasn't had very much yet", name),
                        Recommendation.SEVERITY_NONE
                );
            }
        }
    }

}
