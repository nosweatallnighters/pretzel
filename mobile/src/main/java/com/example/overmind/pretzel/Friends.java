package com.example.overmind.pretzel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.overmind.pretzel.data.FriendProfile;
import java.util.ArrayList;

public class Friends extends Activity {

    ArrayList<FriendProfile> mFriendsList;
    ListView mFriendsListView;
    Button mAddButton;
    BaseAdapter mFriendsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mFriendsListView = (ListView) findViewById(R.id.friends_list);
        mFriendsListView.setEmptyView(findViewById(R.id.no_friends_view));
        mAddButton = (Button) findViewById(R.id.add_friend_button);
        mFriendsList = Global.friendList;
        mFriendsListAdapter = new FriendListAdapter(this, mFriendsList);
        mFriendsListView.setAdapter(mFriendsListAdapter);
        mAddButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                addFriend();
            }
        });
    }


    private void addFriend(){
        FriendProfile newFriend = Global.makeFriend();
        if(newFriend != null){
            mFriendsList.add(0, newFriend);
            mFriendsListAdapter.notifyDataSetChanged();
        } else {
            Snackbar.make(this.mFriendsListView,
                    "Couldn't find any friends to add :(", Snackbar.LENGTH_SHORT).show();
        }

    }


    class FriendListAdapter extends BaseAdapter implements ListAdapter {
        ArrayList<FriendProfile> friendsList;
        Context mContext;
        LayoutInflater mLayoutInflater;

        public FriendListAdapter(Context context, ArrayList<FriendProfile> friends){
            mContext = context;
            friendsList = friends;
            mLayoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return friendsList.size();
        }

        @Override
        public FriendProfile getItem(int position) {
            return friendsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return friendsList.get(position).hashCode();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView;
            if(convertView != null){
                rowView = convertView;
            } else {
                rowView = mLayoutInflater.inflate(R.layout.friend_list_row, parent, false);
            }
            final FriendProfile target = getItem(position);
            if(rowView.getTag() == target){
                return rowView;
            } else {
                ImageView profilePic = (ImageView) rowView.findViewById(R.id.friend_profile_pic);
                final TextView friendName = (TextView) rowView.findViewById(R.id.friend_name_txt);
                TextView friendSub = (TextView) rowView.findViewById(R.id.friend_sub_txt);
                profilePic.setImageResource(target.profileDrawable);
                friendName.setText(target.name);
                friendSub.setText(String.format("BAC: %.3f", target.bac));
                rowView.setTag(target);
                final int friendNumber = position;
                rowView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), FriendProfileActivity.class);
                        intent.putExtra(Global.INTENT_FRIEND_ID, friendNumber);
                        Log.d("Friends", String.format("Firing intent for friend %d: %s", friendNumber, target.name));
                        startActivity(intent);
                    }
                });
                return rowView;
            }
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return friendsList.isEmpty();
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }
    }


}
