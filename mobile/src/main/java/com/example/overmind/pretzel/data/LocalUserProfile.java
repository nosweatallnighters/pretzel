package com.example.overmind.pretzel.data;

import com.example.overmind.pretzel.services.BACCalculatorService;

import java.util.ArrayList;

/**
 * Created by danielpok on 12/9/15.
 */
public class LocalUserProfile extends FriendProfile {
    public final static String SEX_MALE = "MALE";
    public final static String SEX_FEMALE = "FEMALE";

    public String sex;
    public double weightKG;
    public final boolean isLocalUser = true;

    public ArrayList<DrinkRecord> drinks;

    public LocalUserProfile(){
        super();
        drinks = new ArrayList<>();
    }

    @Override
    public double getBAC(){
        return BACCalculatorService.calculateBAC(getDrinkCount(), sex.equals(SEX_MALE),
                weightKG, getDrinkingPeriodHours());
    }

    @Override
    public int getDrinkCount(){
        return drinks.size();
    }

    public double getDrinkingPeriodHours(){
        if(getDrinkCount() == 0){
            return 0;
        } else {
            long firstDrinkTime = drinks.get(drinks.size() - 1).timestamp;
            long lastDrinkTime = drinks.get(0).timestamp;
            long deltaMillis = lastDrinkTime - firstDrinkTime;
            return deltaMillis / (1000.0 * 60 * 60);
        }
    }

    @Override
    public BACCalculatorService.Recommendation getRecommendation(){
        return BACCalculatorService.getRecommendation(getBAC(), isLocalUser, age, name);
    }

}
