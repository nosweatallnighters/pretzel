package com.example.overmind.pretzel.data;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.overmind.pretzel.services.BACCalculatorService;

/**
 * Created by danielpok on 12/8/15.
 */
public class FriendProfile {

    public String name;
    public double bac;
    public int profileDrawable;
    public String profilePath;
    public Bitmap profilePicture;
    public Resources mResources;
    public int drinkCount;
    public int age;
    public final boolean isLocalUser = false;

    public Bitmap getProfilePicture(Resources res){
        mResources = res;
        return getProfilePicture();
    }

    public int getDrinkCount(){
        return drinkCount;
    }

    public Bitmap getProfilePicture(){
        if(profilePicture != null){
            return profilePicture;
        } else if(profileDrawable != 0 && mResources != null){
            return BitmapFactory.decodeResource(mResources, profileDrawable);
        } else if(profilePath != null){
            return BitmapFactory.decodeFile(profilePath);
        } else {
            return null;
        }
    }

    public double getBAC(){
        return bac;
    }

    public BACCalculatorService.Recommendation getRecommendation(){
        return BACCalculatorService.getRecommendation(this);
    }

}
