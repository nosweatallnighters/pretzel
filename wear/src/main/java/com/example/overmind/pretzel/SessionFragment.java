package com.example.overmind.pretzel;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.Button;

/**
 * Created by Lucifer on 12/9/15.
 */
public class SessionFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup sesh_fragment = (ViewGroup) inflater.inflate(
                R.layout.session_fragment, container, false);

        final Button start_sesh = (Button)sesh_fragment.findViewById(R.id.sesh_button);
        start_sesh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddDrinks.class);
                startActivity(i);
            }
        });

        return sesh_fragment;
    }
}
