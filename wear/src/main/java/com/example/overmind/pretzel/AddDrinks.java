package com.example.overmind.pretzel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.wearable.view.GridPagerAdapter;
import android.support.wearable.view.GridViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by junhuizhang on 12/8/15.
 */
public class AddDrinks extends Activity implements OnSharedPreferenceChangeListener {

    private Button wine;
    private Button beer;
    private Button liquor;
    private Button drinks;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_drinks);
        wine = (Button) findViewById(R.id.button2);
        beer = (Button) findViewById(R.id.button4);
        liquor = (Button) findViewById(R.id.button3);

        prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        prefs.registerOnSharedPreferenceChangeListener(this);

        drinks = (Button) findViewById(R.id.numDrinks);
        drinks.setText(String.valueOf(prefs.getInt("drinkcount", 0)));

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefs.edit().putInt("drinkcount", prefs.getInt("drinkcount", 0) + 1).apply();
                Intent i = new Intent();
                i.setAction("com.pretzel.receiver.intent.action.TEST");
                i.putExtra("tag", "/drinkcount");
                i.putExtra("type", "wine");
                i.putExtra("count", prefs.getInt("drinkcount", 0));
                sendBroadcast(i);
            }
        };

        View.OnClickListener listener2 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefs.edit().putInt("drinkcount", prefs.getInt("drinkcount", 0) + 1).apply();
                Intent i = new Intent();
                i.setAction("com.pretzel.receiver.intent.action.TEST");
                i.putExtra("tag", "/drinkcount");
                i.putExtra("type", "beer");
                i.putExtra("count", prefs.getInt("drinkcount", 0));
                sendBroadcast(i);
            }
        };

        View.OnClickListener listener3 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefs.edit().putInt("drinkcount", prefs.getInt("drinkcount", 0) + 1).apply();
                Intent i = new Intent();
                i.setAction("com.pretzel.receiver.intent.action.TEST");
                i.putExtra("tag", "/drinkcount");
                i.putExtra("type", "liquor");
                i.putExtra("count", prefs.getInt("drinkcount", 0));
                sendBroadcast(i);
            }
        };
        wine.setOnClickListener(listener);
        beer.setOnClickListener(listener2);
        liquor.setOnClickListener(listener3);

    }

    public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1)
    {
        Log.d("DEBUG", "Shared Preference Changed");
        if(arg1.equals("drinkcount")) {
            drinks.setText(String.valueOf(arg0.getInt(arg1, 0)));
        }
    }
}
