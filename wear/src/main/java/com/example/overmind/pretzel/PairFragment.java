package com.example.overmind.pretzel;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Lucifer on 12/9/15.
 */
public class PairFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup pair_fragment = (ViewGroup) inflater.inflate(
                R.layout.pair_fragment, container, false);

        final TextView text = (TextView)pair_fragment.findViewById(R.id.pair_text);
        text.setVisibility(View.GONE);

        final ImageView img = (ImageView)pair_fragment.findViewById(R.id.pair_img);

        final Button add_friend = (Button)pair_fragment.findViewById(R.id.add_friend_btn);
        add_friend.setVisibility(View.VISIBLE);
        add_friend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                add_friend.setVisibility(View.GONE);
                text.setText("Shake to Pair");
                img.setImageResource(R.mipmap.shake);
                new CountDownTimer(3000, 1000) {

                    @Override
                    public void onTick(long millisUntilFinished) {
                    }

                    @Override
                    public void onFinish() {
                        text.setText("Pairing");
                        img.setImageResource(R.mipmap.pair);
                        new CountDownTimer(3000, 1000) {

                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                text.setText("Paired!");
                                img.setImageResource(R.mipmap.paired);
                            }
                        }.start();
                    }
                }.start();

            }
        });

        return pair_fragment;
    }
}

