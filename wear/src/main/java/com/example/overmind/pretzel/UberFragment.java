package com.example.overmind.pretzel;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.content.BroadcastReceiver;

/**
 * Created by Lucifer on 12/9/15.
 */
public class UberFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup uber_fragment = (ViewGroup) inflater.inflate(
                R.layout.uber_fragment, container, false);

        ImageView uber = (ImageView)uber_fragment.findViewById(R.id.uber_logo);
        uber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction("com.pretzel.receiver.intent.action.TEST");
                i.putExtra("tag", "/uber");
                getActivity().sendBroadcast(i);
            }
        });

        return uber_fragment;
    }
}
